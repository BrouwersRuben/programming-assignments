package be.kdg.prog1.m8.MorningClass;

public class Car extends Vehicle implements Wheeled{
    protected boolean isElectric;

    public static int WHEELS = 4;
    private static final boolean HAS_ENGINE = true;


    public Car(int topSpeed, boolean isElectric){
        super(topSpeed);
        this.isElectric = isElectric;
    }

    @Override
    public void print(){
        System.out.println("TOP SPEED: " + getTopSpeed());
        System.out.println("ELECTRIC: " + isElectric);
    }

    public boolean isElectric(){
        return isElectric;
    }

    public static boolean hasEngine(){
        return HAS_ENGINE;
    }

    @Override
    public int getNumberOfWheels() {
        return WHEELS;
    }
}
