package be.kdg.prog1.a02Comparable;

public interface Comparable {
    boolean isEqualTo();
    boolean isGreaterThan();
    boolean isLessThan();
}
