package be.kdg.prog1.a02Comparable;

public class Circle extends Shape {

    private int radius;

    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public double getArea() {
        return radius*radius*Math.PI;
    }

    public double getPerimeter() {
        return 2*radius*Math.PI;
    }

    public boolean isEqualTo(Object ob) {
        if(!(ob instanceof Circle)){return false;}
        Circle other=(Circle)ob;
        return this.radius== other.radius;
    }


    public boolean isGreaterThan(Object ob) {
        if(!(ob instanceof Circle)){return false;}
        Circle other=(Circle)ob;
        return this.radius> other.radius;
    }


    public boolean isLessThan(Object ob) {
        if(!(ob instanceof Circle)){return false;}
        Circle other=(Circle)ob;
        return this.radius<other.radius;
    }

    @Override
    public void print() {
        super.print();
    }
}
