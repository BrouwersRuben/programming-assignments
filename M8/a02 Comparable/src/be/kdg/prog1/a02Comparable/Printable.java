package be.kdg.prog1.a02Comparable;

public interface Printable {
    void print();
}
