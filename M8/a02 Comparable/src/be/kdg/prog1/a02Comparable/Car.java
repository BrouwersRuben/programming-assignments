package be.kdg.prog1.a02Comparable;

public class Car implements Comparable, Printable{

    private String brand;
    private String model;
    private String licensePlate;

    public Car(String brand, String model, String licensePlate) {
        this.brand = brand;
        this.model = model;
        this.licensePlate = licensePlate;
    }

    public boolean isEqualTo(Object ob) {
        if(!(ob instanceof Car)){ return false;}
        Car other=(Car)ob;
        return (other.brand.compareTo(this.brand)==0 && other.model.compareTo(this.model)==0);
    }

    public boolean isGreaterThan(Object ob) {
        if(!(ob instanceof Car)){ return false;}
        Car other=(Car)ob;
        if (this.brand.compareTo(other.brand)>0){
            return false;
        } else if (this.model.compareTo(other.model)==0){
            if (this.model.compareTo(other.model)>=0){
                return false;
            }else {return true;}
        } else{
            return true;
        }
    }

    public boolean isLessThan(Object ob) {
        if(!(ob instanceof Car)){ return false;}
        Car other=(Car)ob;
        if (this.brand.compareTo(other.brand)<0){
            return false;
        } else if (this.model.compareTo(other.model)==0){
            if (this.model.compareTo(other.model)<=0){
                return false;
            }else {return true;}
        } else{
            return true;
        }
    }
    @Override
    public void print() {

    }
}
