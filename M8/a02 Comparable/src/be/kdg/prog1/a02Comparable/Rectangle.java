package be.kdg.prog1.a02Comparable;

public class Rectangle extends Shape{

    private int width;
    private int height;

    public Rectangle(int x, int y, int width, int height) {
        super(x, y);
        this.width = width;
        this.height = height;
    }

    public void setDimensions(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public double getArea() {
        return height*width;
    }

    @Override
    public double getPerimeter() {
        return 2*(height*width);
    }

    @Override
    public boolean isEqualTo(Object ob) {
        if(!(ob instanceof Rectangle)){return false;}
        Rectangle other = (Rectangle)ob;
        return (this.height==other.height && this.width==other.width);
    }
    @Override
    public boolean isGreaterThan(Object ob) {
        if(!(ob instanceof Rectangle)){return false;}
        Rectangle other = (Rectangle)ob;
        if (this.width<other.width){
            return false;
        } else if(this.width==other.width){
            if (this.height <= other.height){return false;}
            else {return true;}
        }else {return true;}
    }
    @Override
    public boolean isLessThan(Object ob) {
        if(!(ob instanceof Rectangle)){return false;}
        Rectangle other = (Rectangle)ob;
        if (this.width>other.width){
            return false;
        } else if(this.width==other.width){
            if (this.height >= other.height){return false;}
            else {return true;}
        }else {return true;}
    }

    @Override
    public void print() {
        super.print();
    }
}
