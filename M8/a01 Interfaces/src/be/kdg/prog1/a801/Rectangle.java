package be.kdg.prog1.a801;

public class Rectangle extends Shape {
    private int width;
    private int height;

    public Rectangle(int x, int y, int width, int height) {
        super(x, y);
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public void print() {
        System.out.println(getClass().getSimpleName() + "\n=========");
        System.out.printf("Position: (%d, %d)\nWidth:    %d\nHeight:   %d", getX(), getY(), width, height);
        System.out.println();
    }

    @Override
    public double getArea() {
        return height*width;
    }

    @Override
    public double getPerimeter() {
        return 2*(height*width);
    }
}
