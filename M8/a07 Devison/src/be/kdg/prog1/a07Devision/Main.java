package be.kdg.prog1.a07Devision;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        try {
            System.out.print("Please enter a value for a: ");
            int num1 = sc.nextInt();

            System.out.print("Please enter a value for b: ");
            int num2 = sc.nextInt();

            int division = num1/num2;

            System.out.println("a / b = " + division);
        } catch (ArithmeticException e) {
            System.out.print("You can not divide by zero!");
        }

        /*
        System.out.print("Please enter a value for a: ");
            int num1 = sc.nextInt();

        System.out.print("Please enter a value for b: ");
            int num2 = sc.nextInt();

        try {
            int division = a / b
            System.out.println("a / b = " + division);
        } catch (ArithmeticException e) {
            System.out.print("You can not divide by zero!");
        }
         */

    }
}
