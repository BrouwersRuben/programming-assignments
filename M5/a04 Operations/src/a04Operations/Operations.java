package a04Operations;

public class Operations {
    private float numberOne;
    private float numberTwo;

    public Operations(int numberOne, int numberTwo) {
        this.numberOne = numberOne;
        this.numberTwo = numberTwo;
    }

    public Operations() {
        this(1, 1);
    }

    public float getNumberOne() {
        return numberOne;
    }

    public void setNumberOne(int numberOne) {
        this.numberOne = numberOne;
    }

    public float getNumberTwo() {
        return numberTwo;
    }

    public void setNumberTwo(int numberTwo) {
        this.numberTwo = numberTwo;
    }

    int sum() {
        return (int) (numberOne + numberTwo);
    }

    int difference() {
        return (int) (numberOne - numberTwo);
    }

    int product() {
        return (int) (numberOne * numberTwo);
    }

    double quotient() {
        return (double) numberOne / numberTwo;
    }
/*
    @Override
    public String toString() {
        return "Operations{" +
                "numberOne=" + numberOne +
                ", numberTwo=" + numberTwo +
                '}';
    }
 */
}
