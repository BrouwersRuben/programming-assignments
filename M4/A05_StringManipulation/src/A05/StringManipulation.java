package A05;
import java.util.Scanner;

public class StringManipulation {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String sentence;
        int length;

        System.out.print("Please enter a sentence: ");
        sentence = sc.nextLine();
        System.out.println("This is your sentence in uppercase: " + sentence.toUpperCase());
        System.out.println("This is your sentence in lowercase: " + sentence.toLowerCase());
        System.out.println("In this sentence, every 'a' is replaced by an 'o': " + sentence.replace('a', 'o'));
        System.out.println("This is the length of your string : " + sentence.length());
        length = sentence.length();
        System.out.println("This is the first letter of your sentence: " + sentence.charAt(0));
        System.out.println("This is the last letter of your sentence: " + sentence.charAt(length-1));
        //System.out.println("This is the amount of 'e' appears in your sentence: " + sentence);
        String noEs = sentence.replace("e", "");
        int numE = length - noEs.length();
        System.out.println("This is the amount of e's in this sentence: "+ numE);







    }
}
