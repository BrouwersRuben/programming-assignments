package A04;

public class StringTests {
    public static void main(String[] args) {

        String one = "abc";
        String two = "abc";
        String three = new String("abc");

        System.out.println(one == two);
        System.out.println(one == three);
        System.out.println(two == three);

        System.out.println("");

        System.out.println(one.equals(two));
        System.out.println(one.equals(three));
        System.out.println(two.equals(three));
    }
}
//Equals: evaluates to the comparison of values in the objects.
//== : hecks if both objects point to the same memory location
