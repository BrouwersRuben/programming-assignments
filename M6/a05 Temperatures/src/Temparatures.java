import java.sql.SQLOutput;
import java.util.Scanner;

public class Temparatures {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double[] temperatures= new double[7];
        double average = 0;

        for(int i = 0; i < temperatures.length; i++) {
            System.out.print("Enter the temperature of day " + (i+1) + ": ");
            temperatures[i] = sc.nextDouble();
            average += temperatures[i];
        }
        average /= temperatures.length;

        System.out.print("Summary: \n============== \n");
        for(int i = 0; i < temperatures.length; i++) {
            System.out.printf("%-10s"+ temperatures[i], "Day " + (i+1) + ": ");
            System.out.println();
        }
        System.out.println("==============");
        System.out.printf("Average: %.2f ", average);
    }
}
