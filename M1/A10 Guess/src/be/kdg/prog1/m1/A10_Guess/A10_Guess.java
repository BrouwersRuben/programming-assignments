package be.kdg.prog1.m1.A10_Guess;

import java.util.Scanner;

public class A10_Guess {
    public static void main(String[] args) {
        int guess;
        int secret = 54;

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Enter a number: ");
        guess = keyboard.nextInt();
        while (true) {
            if ( guess == secret ) {
                System.out.println("Congrats, you have guessed the number ");
                return;
            }
            if (guess < secret) {
                System.out.println("Higher!");
            }
            if (guess > secret) {
                System.out.println("Lower!");
            }
        }

    }
}
