package be.kdg.prog1.m1;

import java.util.Scanner;

public class A10_Summation {
	public static void main(String[] args) {

		Scanner keyboard = new Scanner(System.in);

		int number = 1;
		int sum = 0;

		boolean answer = true;


		while (answer) {

			while (number != 0){
				System.out.print("Please enter a number (0 stops): ");
				 if (keyboard.hasNextInt()) {
				 	number = keyboard.nextInt();
					sum = sum + number;
				 } else {
					 System.out.println("You have not entered a number! ");
				 	System.exit(0);
				 }
			}

			System.out.println("The sum of all your numbers is " + sum);
			System.out.println(" ");
			System.out.print("Would you like to continue? (yes or no): ");
			String UserChoice  = keyboard.next();
			if (!UserChoice.equals("yes")) answer = false;

			}
		System.out.println("The program has ended");
		System.exit(0);
		}
	}
