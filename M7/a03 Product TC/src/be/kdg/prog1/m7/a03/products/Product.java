package be.kdg.prog1.m7.a03.products;

public class Product {
    protected String code;
    protected String description;
    protected float price;

    public Product(String code, String description, float price) {
        this.code = code;
        this.description = description;
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public double getVat() {
        return price * 0.21;
    }

    public double getPrice(){
        return price +getVat();
    }
}

