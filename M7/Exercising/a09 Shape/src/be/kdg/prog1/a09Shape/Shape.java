package be.kdg.prog1.a09Shape;

public class Shape {
    private int x;
    private int y;

    public Shape(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setPosition(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getArea(){
        return x*y;
    }

    public int getPerimeter(){
        return 2*(x+y);
    }
}
