package be.kdg.prog1.a07Dog.animals;

import org.w3c.dom.ls.LSOutput;

public class Test {
    public static void main(String[] args) {
        Dog dog1 = new Dog("Riva", "Rhodesian Ridgeback", "Brown", "12345");
        System.out.println(dog1);
        Rabbit rabbit1 = new Rabbit("Flappie", "Ground Digger", "White", "12345", true);
        System.out.println(rabbit1);
    }
}
