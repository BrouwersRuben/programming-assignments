package be.kdg.prog1.demo7;

public class Person {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age < 0){
            this.age = age;
        }
    }
    public void print(){
        System.out.printf("%s %d", getName(), getAge());
    }
    @Override
    public String toString() {
        return " NAME: " + name + ";AGE: " + age;
    }
}
