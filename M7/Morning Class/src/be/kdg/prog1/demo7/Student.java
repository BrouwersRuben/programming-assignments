package be.kdg.prog1.demo7;

public class Student extends Person{
    /*private String name;
    private int age;*/
    private String studentId;
    private int graduatingYear;

    public Student(String studentId, int graduatingYear) {
        this.studentId = studentId;
        this.graduatingYear = graduatingYear;
    }

    public String getStudentId() {
        return studentId;
    }

    public int getGraduatingYear() {
        return graduatingYear;
    }
    public void print(){
        super.print();
        System.out.printf(" %s %d%n", getStudentId(), getGraduatingYear());
    }
    @Override
    public String toString() {
        return super.toString() + "STUDENT ID: " + studentId + "GRADUATING YEAR" + graduatingYear;
    }
}
