package be.kdg.prog1.demo7;

public class Teacher extends Person{ //Teacher is a kind of Person
    private long teacherId;
    private double salary;

    public Teacher(String name, int age, long teacherId, double salary) {
        /*this.name = name;
        this.age = age;*/
        this.teacherId = teacherId;
        this.salary = salary;
    }
/*
    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age<0) {
            this.age = age;
        }else{//normally we do not print error messages in setters
            System.out.println("Error");
        }
    }
 */

    public long getTeacherId() {
        return teacherId;
    }

    public double getSalary() {
        return salary;
    }

    public void print(){
        super.print();
        System.out.printf(" %d %.2f%n", getTeacherId(), getSalary());
    }
    @Override
    public String toString() {
        return super.toString() + "TEACHER ID: " + teacherId + ";SALARY: " + salary;
    }
}
