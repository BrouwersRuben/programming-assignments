package a14_studentContact.person;

public class Person {
    private String name;
    private Contact contact;

    public Person(String name, String email, String fixedNumber, String mobileNumber) {
        this.contact = new Contact(email, fixedNumber, mobileNumber);
        this.name = name;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null || getClass() != object.getClass()) return false;
        Person person = (Person) object;
        return contact == person.contact;
    }

    @Override
    public int hashCode() {
        return contact.hashCode()^ name.hashCode();
    }

    @Override
    public String toString() {
        return String.format("\nname: %s\n%s", name, contact);
    }
}
