package a14_studentContact.person;

public class Contact {
    private String email;
    private Phone fixed, mobile;

    public Contact(String email, String mobileNumber, String fixedNumber) {
        this.email = email;
        this.fixed = new Phone("fixed",fixedNumber);
        this.mobile = new Phone("mobile", mobileNumber);
    }

    @Override
    public String toString() {
        return String.format("email: %s\nmobile phone: %s\n fixed phone: %s", email, mobile, fixed);
    }
}
