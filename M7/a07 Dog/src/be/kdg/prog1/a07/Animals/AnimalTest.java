package be.kdg.prog1.a07.Animals;

public class AnimalTest {
    public static void main(String[] args) {
        Dog Riva = new Dog("123456789", "Riva", "Ridgeback", "Brown");
        Rabbit Flappie = new Rabbit ("Flappie", "Dutch Giant", "white with black spots", true);

        System.out.println(Riva.toString());
        System.out.println(Flappie.toString());
    }
}
