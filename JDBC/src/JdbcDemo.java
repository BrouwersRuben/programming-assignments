import Domain.Player;

import java.sql.*;

public class JdbcDemo {
    public static void main(String[] args) {
        try {
            Connection cx = DriverManager.getConnection("jdbc:hsqldb:file:db/demoDB", "sa", "");
            Statement stmt = cx.createStatement();
            stmt.execute("CREATE TABLE IF NOT EXISTS " +
                    "players (player_name VARCHAR(10), top_score INTEGER);");
            ResultSet rows = stmt.executeQuery("SELECT PLAYER_NAME, TOP_SCORE FROM PLAYERS;");
            while(rows.next()){
                String name = rows.getString("PLAYER_NAME");
                int score = rows.getInt("TOP_SCORE");
                //System.out.println("Name: " + name + ", Score: " + score);
                Player player = new Player(name, score);
                System.out.println("read: " + player);
            }
            int changes =stmt.executeUpdate("UPDATE PLAYERS " +
                            "SET TOP_SCORE=3000;" +
                    "WHERE PLAYER_NAME LIKE '%Ruben%'");
            System.out.printf("Updated %d row \n", changes);
            stmt.close();
            //if you close the connection, it also closes the statement, so not so necessary
            cx.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }
}
