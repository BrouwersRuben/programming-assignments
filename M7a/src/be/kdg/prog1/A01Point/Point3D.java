package be.kdg.prog1.a01;

public class Point3D extends Point{
    private int z;

    public int getZ() {
        return z;
    }

    public Point3D(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }
}
