package be.kdg.prog1.a02PointtoString;

public class Point3D extends Point {
    private int z;



    public int getZ() {
        return z;
    }

    public Point3D(int x, int y, int z) {
        super(x, y);
        this.z = z;
    }

    public Point3D() {

    }

    public void setZ(int z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return super.toString() + " z: " + z;
    }
}
