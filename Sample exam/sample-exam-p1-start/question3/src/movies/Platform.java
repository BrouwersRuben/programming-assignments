package movies;

public class Platform {
    private final int CAPACITY = 20;
    private Movie[] movies = new Movie[CAPACITY];
    private int next = 0;

    public void addMovie(Movie movie) {
        movies[next] = movie;
        next++;
    }

    public Movie getMovie(int index){
        return movies[index];
    }

    int getNumberOfMovies(){
        int count = 0;
        for(int i = 0; i <= movies.length; i++){
            if (i !=0){
                count++;
            }
        }
        return count;
    }

    boolean isFull(){
        boolean isFull = false;
        for (Movie movie : movies) {
            if (movie == null) {
                isFull = true;
                break;
            }
        }
        return  isFull;
    }

}
